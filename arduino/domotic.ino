/*
  Switch statement with serial input

  Demonstrates the use of a switch statement. The switch statement allows you
  to choose from among a set of discrete values of a variable. It's like a
  series of if statements.

  To see this sketch in action, open the Serial monitor and send any character.
  The characters a, b, c, d, and e, will turn on LEDs. Any other character will
  turn the LEDs off.

  The circuit:
  - five LEDs attached to digital pins 2 through 6 through 220 ohm resistors

  created 1 Jul 2009
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/SwitchCase2
*/

struct lamp {
  int pin;
  bool state;
};

lamp lamp1 = {2, false};
lamp lamp2 = {3, false};

double Light (int RawADC0)
{
  double Vout=RawADC0*0.0048828125;
  //int lux=500/(10*((5-Vout)/Vout));//use this equation if the LDR is in the upper part of the divider
  int lux=(2500/Vout-500)/10;
  return lux;
}

void setup() {
  Serial.begin(9600);

  //lamp1
  pinMode(lamp1.pin, OUTPUT);
  digitalWrite(lamp1.pin, LOW);

  //lamp2
  pinMode(lamp2.pin, OUTPUT);
  digitalWrite(lamp2.pin, LOW);
}

void loop() {
 
  if (Serial.available() > 0) {
    int inByte = Serial.read();
    
    // do something different depending on the character received.
    // The switch statement expects single number values for each case; in this
    // example, though, you're using single quotes to tell the controller to get
    // the ASCII value for the character. For example 'a' = 97, 'b' = 98,
    // and so forth:

    switch (inByte) {
      case 'a':
        if(lamp1.state) {
          digitalWrite(lamp1.pin, LOW);
          lamp1.state = false;
        } else {
          digitalWrite(lamp1.pin, HIGH);  
          lamp1.state = true;
        }
        
        break;
      case 'b':
        if(lamp2.state) {
          digitalWrite(lamp2.pin, LOW);
          lamp2.state = false;
        } else {
          digitalWrite(lamp2.pin, HIGH);  
          lamp2.state = true;
        }

        break;
      case 'c':
        Serial.println(lamp1.state);
        break;
      case 'd':
        Serial.println(lamp2.state);
        break;
      case 'e':
        Serial.print(int(Light(analogRead(0))));       
        break;
      default:
        Serial.println("nothing");
    }
  }
}
