<?php

include 'php_classes/PhpSerial.php';

/**
* Home class lets you handle house components such as lights and fans; you
* can also get informations such as temperature or lumens.
* @author Yasser Charef
* @version 1.0
* @copyright under GPL 2 licence
*/

class Home {


	/**
	* turns a selected light on
	* @param $numb number of the light
	*/
	function switchLight($numb) {

		if($numb == 1){
			shell_exec('python file.py a');
		} elseif($numb == 2) {
			shell_exec('python file.py b');
		}

		$serial->deviceClose();
	}

	/**
	* returns an array with lamps info
	* @return array with lamp info
	*/
	function getLampInfo() {
		$lamp[0] = intval(shell_exec('python file.py c'));
		$lamp[1] = intval(shell_exec('python file.py d'));

		return $lamp;
	}

	/**
	* return the current temperature
	* @return temperature
	*/
	function getTemperature() {
		return 25;
	}

	/**
	* returns the current luminosity in lumen
	* @return luminosity
	*/
	function getLuminosity() {
		$lumen = intval(shell_exec('python file.py e'));
		if($lumen >10000)
			return 'low';
		else if($lumen >500)
			return 'medium';
		else if($lumen < 500)
			return 'high';

	//	return shell_exec('python file.py e');
	}

	/**
	* returns the current humidity
	* @return humudity
	*/
	function getHumidity() {
		return 20;
	}

	/**
	* returns a json object with temperature info
	* @return JsonObject
	*/

	function getXMLTemperature() {
		$servername = "localhost";
		$username = "root";
		$password = "";

		// Create connection
		$conn = new mysqli($servername, $username, $password);

		// Check connection
		if ($conn->connect_error) {
			echo "Connection failed: " . $conn->connect_error;
		}

		$sql = "SELECT TIME_FORMAT(`Istante`, '%H:%i')
		 				AS Istante, Temperatura
		 				FROM domotica.temperatura";

		$result = $conn->query($sql);

		$outp = array();
		$outp = $result->fetch_all(MYSQLI_ASSOC);

		$conn->close();

		return json_encode($outp);

	}

}
