//var lgt = setInterval(updateLgt, 1000);
var tmp = setInterval(updateTmp, 1000);
var hmd = setInterval(updateHmd, 1000);
var lum = setInterval(updateLmn, 4000);
var chrt = setInterval(updateChrt, 1000);
var lamp = setInterval(updateLamps, 500);

// Load the Visualization API and the corechart package.
google.charts.load('current', {
  'packages': ['corechart']
});

function drawChart(myObj) {

  var data = new google.visualization.DataTable();

  data.addColumn('string', 'orario');
  data.addColumn('number', 'temperatura');


  for (i = 0; i <myObj.length; i++) {
    //alert();
    data.addRow([myObj[i].Istante, parseInt(myObj[i].Temperatura)]);
  }

  // Set chart options
  var options = {
    'title': 'Consumi elettrici',
    'width': 400,
    'height': 300
  };

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}


function updateLamps() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
      var myObj = JSON.parse(this.responseText);
      document.getElementById("lgt").innerHTML =
        myObj[0] + myObj[1];

      setLamp("cucina", myObj[0]);
      setLamp("salotto", myObj[1]);
      setLamp("matrimoniale", 0);
      setLamp("singola", 0);
      setLamp("bagno", 0);
      setLamp("corridoioS", 0);
      setLamp("corridoioC", 0);
      setLamp("entrata", 0);
    }

  };
  xhttp.open("GET", "data.php?request=lampinfo", true);
  xhttp.send();
}

function setLamp(room, state) {
  if(state == 1) {
    document.getElementById(room).style.color = "yellow";
  } else if(state == 0) {
    document.getElementById(room).style.color = "white";
  } else {
    document.getElementById(room).style.color = "gray";
  }

}

function updateChrt() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
      var myObj = JSON.parse(this.responseText);
      drawChart(myObj);
    }

  };
  xhttp.open("GET", "data.php?request=tmpdata", true);
  xhttp.send();
}

function updateTmp() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("tmp").innerHTML =
        this.responseText;
    }

  };
  xhttp.open("GET", "data.php?request=temperature", true);
  xhttp.send();
}


function updateHmd() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("hmd").innerHTML =
        this.responseText;
    }

  };
  xhttp.open("GET", "data.php?request=humidity", true);
  xhttp.send();
}

function updateLmn() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("lmn").innerHTML =
        this.responseText;
    }

  };
  xhttp.open("GET", "data.php?request=luminosity", true);
  xhttp.send();
}
